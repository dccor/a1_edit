package com.editor;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JPanel;
/**
 * FlowPanel
 * @author kos
 *
 */
public class FlowPanel extends JPanel implements ActionListener {

    private static final long serialVersionUID = 1L;

    /**
     * 3 Buttons
     */
    protected JButton b1, b2, b3;

    public FlowPanel() {
	setLayout(new FlowLayout());
	
	/**
	 * create Buttons and set Position
	 */
	b1 = new JButton("Paste");
    b1.setVerticalTextPosition(AbstractButton.CENTER);
    b1.setHorizontalTextPosition(AbstractButton.LEADING); //aka LEFT, for left-to-right locales
//    b1.setMnemonic(KeyEvent.VK_D);
    b1.setActionCommand("enable");

    b2 = new JButton("Delete");
    b2.setVerticalTextPosition(AbstractButton.BOTTOM);
    b2.setHorizontalTextPosition(AbstractButton.CENTER);
//    b2.setMnemonic(KeyEvent.VK_D);

    b3 = new JButton("Copy");
    //Use the default text position of CENTER, TRAILING (RIGHT).
//    b3.setMnemonic(KeyEvent.VK_C);
    b3.setActionCommand("enable");
    
    
    /**
     * Listen for actions on buttons 1,2 and 3.
     */
    b1.addActionListener(this);
    b2.addActionListener(this);
    b3.addActionListener(this);
    
    b1.setToolTipText("Click this button to paste from Clipboard.");
    b2.setToolTipText("Click this button to delete Text Editor.");
    b3.setToolTipText("Click this button to copy Text to Clipboard.");

    /**
     * Add Components to this container, using the default FlowLayout. 
     */
    add(b1);
    add(b2);
    add(b3);
    }
    

    /**
     * ActionEvent 3 Buttons -  Action for paste, delete, copy Text
     */
    @Override
    public void actionPerformed(ActionEvent e) {
    	if(e.getSource() == this.b1){
    		EditorFenster.getInstance().pasteTextFromClipboard();
    		EditorFenster.getInstance().setStatusLabel("Text Einfügen...");
        }
        else if(e.getSource() == this.b2){
        	EditorFenster.getInstance().deleteText();
        	EditorFenster.getInstance().setStatusLabel("Text Löschen...");
        }
        else if (e.getSource() == this.b3){
        	EditorFenster.getInstance().copyTextToClipboard();
        	EditorFenster.getInstance().setStatusLabel("Text Kopieren...");
        }
    }

}
