package com.editor;

import java.awt.Color;
import java.awt.DefaultFocusTraversalPolicy;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Spring;
import javax.swing.SpringLayout;

/**
 * CommandsPanel JPanel
 * @author kos
 *
 */
public class CommandsPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	/**
	 * 5 TextFields
	 */
	public JTextField txtCom1, txtCom2, txtCom3, txtCom4, txtCom5;

	/**
	 * Process Button
	 */
	final JButton jb;

	public CommandsPanel() {

		JLabel lbl1 = new JLabel("");
		lbl1.setForeground(Color.BLUE);
		lbl1.setOpaque(true);
		jb = new JButton("Process");
		jb.setBackground(Color.YELLOW);
		jb.setForeground(Color.BLACK);
		jb.setActionCommand("enable");
		jb.setMnemonic(KeyEvent.VK_ENTER);
		// set default Button (Enter Key)
		EditorFenster.getInstance().getRootPane().setDefaultButton(jb);

		final int cols = 2, rows = 6;
		SpringLayout layout;
		setLayout(layout = new SpringLayout());

		// add all Components
		add(createLabelFont(40, 30, 200, 30, "Eingabe:", new Font("Serif", Font.BOLD + Font.ITALIC, 18)));

		this.add(lbl1);

		/**
		 * TextFields with Labels
		 */
		add(createLabel(30, 80, 60, 20, "Command 1"));
		add(txtCom1 = createTextField(90, 80, 200, 20));

		add(createLabel(30, 105, 60, 20, "Command 2"));
		add(txtCom2 = createTextField(90, 105, 200, 20));

		add(createLabel(30, 105, 60, 20, "Command 3"));
		add(txtCom3 = createTextField(90, 105, 200, 20));

		add(createLabel(30, 105, 60, 20, "Command 4"));
		add(txtCom4 = createTextField(90, 105, 200, 20));

		add(createLabel(30, 105, 60, 20, "Command 5"));
		add(txtCom5 = createTextField(90, 105, 200, 20));

		/**
		 * Button Process Action Listener
		 */
		jb.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				EditorFenster.getInstance().setStatusLabel("Process...");
				// clear Text Editor
				EditorFenster.getInstance().deleteText();
				// read commands from TextField
				EditorFenster.getInstance().getCommands().readCommands();

				if (EditorFenster.getInstance().getVrfList().getVrfList().isEmpty()) {
					EditorFenster.getInstance().setStatusLabel("Keine Liste geladen!");
				} else {
					// EditorFenster.getInstance().getVrfList().writeVrfList(System.out);
					// EditorFenster.getInstance().getVrfList().writeNextVrfList(System.out);

					writeOutput();
				}


			}
		});

		this.add(jb);

		/**
		 * Align Button jb
		 */
		layout.putConstraint(SpringLayout.EAST, jb, -20, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, jb, 20, SpringLayout.SOUTH, this);

		/**
		 * Align all cells in each column and make them the same width.
		 */
		int initialX = 10, initialY = 6, xPad = 16, yPad = 16;

		Spring x = Spring.constant(initialX);
		for (int c = 0; c < cols; c++) {
			Spring width = Spring.constant(0);
			for (int r = 0; r < rows; r++) {
				width = Spring.max(width, layout.getConstraints(getComponent(r * cols + c)).getWidth());
			}
			for (int r = 0; r < rows; r++) {
				SpringLayout.Constraints constraints = layout.getConstraints(getComponent(r * cols + c));
				constraints.setX(x);
				constraints.setWidth(width);
			}
			x = Spring.sum(x, Spring.sum(width, Spring.constant(xPad)));
		}

		/**
		 * Align all cells in each row and make them the same height.
		 */
		Spring y = Spring.constant(initialY);
		for (int r = 0; r < rows; r++) {
			Spring height = Spring.constant(24);
			for (int c = 0; c < cols; c++) {
				SpringLayout.Constraints constraints = layout.getConstraints(getComponent(r * cols + c));
				constraints.setY(y);
				constraints.setHeight(height);
			}
			y = Spring.sum(y, Spring.sum(height, Spring.constant(yPad)));
		}

		/**
		 * Set the parent's size.
		 */
		SpringLayout.Constraints pCons = layout.getConstraints(this);
		pCons.setConstraint(SpringLayout.SOUTH, y);
		pCons.setConstraint(SpringLayout.EAST, x);

		setFocusTraversalPolicy(new DefaultFocusTraversalPolicy());
	}
	
	
	/**
	 * write Output in EditorPane with foreach and StringBuilder
	 */
	public void writeOutput(){
		if (EditorFenster.getInstance().getVrfList().getVrfList() != null) {
			StringBuilder builder = new StringBuilder();
			for (String comm : EditorFenster.getInstance().getCommands().getCommandList()) {
				for (String st : EditorFenster.getInstance().getVrfList().getVrfList()) {
					builder.append(comm).append(" ").append(st).append("\n");
				}
			} 
			EditorFenster.getInstance().insertText(builder.toString());
		}
		else{
			JOptionPane.showMessageDialog(null, "Liste konnte nicht geladen werden!", "Information Message",
					JOptionPane.INFORMATION_MESSAGE);
		}
	}
	

	/**
	 * create Labels with given Parameters for alignment
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @param text
	 * @return label
	 */
	private JLabel createLabel(int x, int y, int w, int h, String text) {
		// ein Label mit dem Text erzeugen
		JLabel label = new JLabel(text);
		label.setBounds(x, y, w, h);
		return label;
	}

	/**
	 * create Label with given Parameters for alignment and Font
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @param text
	 * @param font
	 * @return label
	 */
	private JLabel createLabelFont(int x, int y, int w, int h, String text, Font font) {
		JLabel label = createLabel(x, y, w, h, text);
		label.setFont(font);
		return label;
	}

	/**
	 * create TextFields with given Parameters for alignment
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @return
	 */
	private JTextField createTextField(int x, int y, int w, int h) {
		JTextField txt = new JTextField(12);
		txt.setBounds(x, y, w, h);
		// in Tab-reihenfolge aufnehmen
		txt.setFocusable(true);
		return txt;
	}

	/**
	 * get the txtCom1 TextField, for focus set
	 * @return JTextField
	 */
	public JTextField getTxtCom1() {
		return txtCom1;
	}

}
