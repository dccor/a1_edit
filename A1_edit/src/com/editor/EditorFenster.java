package com.editor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import com.model.Commands;
import com.model.MenueHandler;
import com.model.VrfList;

/**
 * Main Class - Swing Window 
 * Date: 25.07.2017
 * @author kos
 * @version 1.0
 * git a1_editor master  
 */
public class EditorFenster extends JFrame {

	private static final long serialVersionUID = 1L;

	final JMenuItem mNew, mOpen, mLoad, mSaveAs, mExit, mCopy, mPaste;
	private JEditorPane displayPane;
	private JPanel statusPanel;
	JLabel statusLabel;
	
	@SuppressWarnings("unused")
	private FlowPanel flowPanel;
	private CommandsPanel commandsPanel;
	private Commands commands;
	private VrfList vrflist;

	// TODO: check if needed (for Ant build) ?????????
	private BufferedImage icon;
	/**
	 * static EditorFenster instance;
	 */
	private static EditorFenster instance;

	/**
	 * static singleton Instance of EditorClass
	 * @return instance
	 */
	public static EditorFenster getInstance() {
		return instance;
	}

	/**
	 * Construct EditorFenster
	 */
	public EditorFenster() {
		super("Tom Commander");
		// Icon for JFrame		
//		BufferedImage image = null;  // image NOT used in this - for build-TEST
		
//			try {
////	        	image = ImageIO.read(getClass().getClassLoader().getResource("com/img/Router.png"));
////				this.icon = ImageIO.read(new FileInputStream("com/img/Router.PNG"));
//	        	this.icon = ImageIO.read(this.getClass().getResourceAsStream("/com/img/Router.png"));
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			if (icon != null) {
//			this.setIconImage(icon);
//		}else{
//			JOptionPane.showMessageDialog(null, "Icon Image konnte nicht geladen werden!", "Information Message",
//					JOptionPane.INFORMATION_MESSAGE);
//		}
		
		
		
		/**
		 * Groesse, Location, Layout
		 */
		this.setSize(800, 500);
		this.setLocation(100, 100);
		this.setLayout(new BorderLayout());
		this.setMinimumSize(new Dimension(250, 150));
		this.setMaximumSize(Toolkit.getDefaultToolkit().getScreenSize());

		instance = this;
		
		add(displayPane = new JEditorPane(), BorderLayout.CENTER);
		displayPane.setEditable(true);
		/**
		 * Status Bar with Label
		 */
		add(statusPanel = new JPanel(),BorderLayout.SOUTH);
		statusPanel.setPreferredSize(new Dimension(this.getWidth(), 20));
		statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.X_AXIS));
		statusLabel = new JLabel("Status");
		statusLabel.setHorizontalAlignment(SwingConstants.LEFT);
		statusPanel.add(statusLabel);

		add(flowPanel = new FlowPanel(), BorderLayout.NORTH);
		add(commandsPanel = new CommandsPanel(), BorderLayout.WEST);

		JScrollPane scroll = new JScrollPane(displayPane);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		/**
		 * add Textarea Container
		 */
		getContentPane().add(scroll);

		/**
		 * Handler for Menue items
		 */
		MenueHandler handler = new MenueHandler();
		// new MenuBar
		JMenuBar mBar = new JMenuBar();
		// new Menü Datei
		JMenu mDatei = new JMenu("Datei");
		mDatei.setMnemonic(KeyEvent.VK_D);
		mBar.add(mDatei);

		/**
		 * MenuItems: Neu,Öffnen,Speichern,Speichern unter, Beenden
		 * KeyStroke Tastenkombination(mit STRG)
		 */
		mNew = createMenuItem("Neu", "NEW", KeyEvent.VK_N, KeyStroke.getKeyStroke('N', InputEvent.CTRL_DOWN_MASK),
				handler);
		mDatei.add(mNew);
		mOpen = createMenuItem("Öffnen", "OPEN", KeyEvent.VK_O, KeyStroke.getKeyStroke('O', InputEvent.CTRL_DOWN_MASK), 
				handler);
		mDatei.add(mOpen);
		mLoad = createMenuItem("Liste laden", "LOAD", KeyEvent.VK_L,
				KeyStroke.getKeyStroke('L', InputEvent.CTRL_DOWN_MASK),handler);
		mDatei.add(mLoad);
//		mSave = createMenuItem("Speichern", "SAVE", KeyEvent.VK_S,
//				KeyStroke.getKeyStroke('S', InputEvent.CTRL_DOWN_MASK), handler);
//		mDatei.add(mSave);
		mSaveAs = createMenuItem("Speichern unter...", "SAVEAS", KeyEvent.VK_S,
				KeyStroke.getKeyStroke('S', InputEvent.CTRL_DOWN_MASK), handler);
		mDatei.add(mSaveAs);
		mExit = createMenuItem("Beenden", "EXIT", KeyEvent.VK_B, KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0), handler); 
		// KeyStroke																										// F12-Taste)
		mDatei.add(mExit);

		/**
		 * MenuItems Bearbeiten, Kopieren, Einfügen
		 */
		JMenu mEdit = new JMenu("Bearbeiten");
		mBar.add(mEdit);
		mCopy = createMenuItem("Kopieren", "COPY", handler);
		mEdit.add(mCopy);
		mPaste = createMenuItem("Einfügen", "PASTE", handler);
		mEdit.add(mPaste);

		this.setJMenuBar(mBar);
		/**
		 * set DefaultCloseOperation
		 */
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		/**
		 * own Close-Aktion - NOT used in this Version
		 */
//		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

		// WindowListener für schließen:
//		addWindowListener(new WindowAdapter() {
//			@Override
//			public void windowClosing(WindowEvent e) {
//				// auf mein beenden umleiten
//				mExit.doClick();
//			}
//		});
		
		/**
		 * new VrfList()
		 * new Commands()
		 */
		vrflist = new VrfList();
		commands = new Commands();
	}


	/**
	 * create MenuItem
	 * @param text
	 * @param action
	 * @param handler
	 * @return item
	 */
	private JMenuItem createMenuItem(String text, String action, ActionListener handler) {
		JMenuItem item = new JMenuItem(text);
		item.setActionCommand(action);
		// ... und ActionListener registrieren
		item.addActionListener(handler);
		return item;
	}

	/**
	 * create MenuItem with mnemonic
	 * @param text
	 * @param action
	 * @param mnemonic
	 * @param keyStroke
	 * @param handler
	 * @return item
	 */
	private JMenuItem createMenuItem(String text, String action, int mnemonic, KeyStroke keyStroke,
			MenueHandler handler) {
		JMenuItem item = createMenuItem(text, action, handler);
		// mnemonic setzen
		item.setMnemonic(mnemonic);
		// Tastaturshortcut setzen
		item.setAccelerator(keyStroke);
		return item;
	}

	/**
	 * get selcted Text from Editor
	 * @return text
	 */
	public String getSelectedText() {
		return displayPane.getSelectedText();
	}

	/**
	 * get Text from Editor if not null - otherwise return: ERROR
	 * @return text
	 */
	public String getText() {
		if(displayPane.getText() != null){
		return displayPane.getText();
		}
		else
			return "ERROR - NO TEXT";
	}
	
	/**
	 * copy all Text to Clipboard
	 */
	public void copyTextToClipboard(){
		setStatusLabel("in Zwischenablage Kopieren...");
		StringSelection stringSelection = new StringSelection (displayPane.getText());
		Clipboard clpbrd = Toolkit.getDefaultToolkit ().getSystemClipboard ();
		clpbrd.setContents (stringSelection, null);
	}
	
	/**
	 * paste from Clipboard to TextEditor
	 */
	public void pasteTextFromClipboard(){
		setStatusLabel("Aus Zwischenablage Einfügen...");
		Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
		if (clpbrd != null) {
			String string;
			try {
				string = (String) clpbrd.getData(DataFlavor.stringFlavor);
				displayPane.setText(string);
			} catch (UnsupportedFlavorException | IOException e) {
				e.printStackTrace();
			} 
		}
	}

	/**
	 * set Text to Editor
	 * @param content String
	 */
	public void setText(String content) {
		displayPane.setText(content);
	}
	
	/**
	 * delete Text
	 */
	public void deleteText(){
		displayPane.setText("");
	}

	/**
	 * insert or replace text to Editor
	 * @param content String
	 */
	public void insertText(String content) {
		displayPane.replaceSelection("");
		try {
			displayPane.getDocument().insertString(displayPane.getCaretPosition(), content, null);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * append Text to Editor
	 * @param s
	 * cath BadLocationException
	 */
	public void appendText(String s) {
		   try {
		      Document doc = displayPane.getDocument();
		      doc.insertString(doc.getLength(), s, null);
		   } catch(BadLocationException exc) {
		      exc.printStackTrace();
		   }
		}
	
	/**
	 * get VrfList Class Object
	 * @return vrfList
	 */
	public VrfList getVrfList(){
		return vrflist;
	}
	
	/**
	 * get Commands Class Object
	 * @return commands
	 */
	public Commands getCommands(){
		return commands;
	}
	
	/**
	 * get Commands Panel Class
	 * @return commandsPanel
	 */
	public CommandsPanel getCommandsPanel(){
		return commandsPanel;
	}

	/**
	 * gets and sets Label-String in StatusBar
	 * @return statusLabel
	 */
	public JLabel getStatusLabel() {
		return statusLabel;
	}
	
	public void setStatusLabel(String statusLabel) {
		this.statusLabel.setText(statusLabel);
	}

	public static void main(String[] args) {
		EditorFenster dasFenster = new EditorFenster();
		dasFenster.setVisible(true);
		dasFenster.getCommandsPanel().getTxtCom1().requestFocusInWindow();
	}

}
