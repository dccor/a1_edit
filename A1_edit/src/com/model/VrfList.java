package com.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
/**
 * VrfList
 * @author kos
 *
 */
public class VrfList {
	/**
	 * List holds vrfList
	 */
	private List<String> vrfList;

	/**
	 * Construktor creates new ArrayList
	 */
	public VrfList() {
		vrfList = new ArrayList<String>();
	}

	/**
	 * read vrfList line by line from File to ArrayList with FileInputStream()
	 * @param file File
	 * catch FileNotFoundException, IOException
	 */
	public void readVrfList(File file) {
		vrfList.clear();
		FileInputStream fstream;
		try {
			fstream = new FileInputStream(file);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String strLine;
			// Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				this.vrfList.add(strLine);
			}
			// Close the input stream
			br.close();
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "vrf List File konnte nicht geladen werden!", "Error Massage",
					JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, "vrf List File konnte nicht geladen werden!", "Error Massage",
					JOptionPane.ERROR_MESSAGE);
			ex.printStackTrace();
		}
	}
	
	
	// write List line .next line
//	public void writeNextVrfList(PrintStream p){
//		if (vrfList != null && p != null){
//			StringBuilder builder = new StringBuilder();
//			int VrfListSize = EditorFenster.getInstance().getVrfList().getVrfList().size();
//			int commandListSize = EditorFenster.getInstance().getCommands().getCommandList().size();
//			for(int i = 0; i < VrfListSize;){
//				for(int j = 0; j < commandListSize; j++){
//					builder.append(EditorFenster.getInstance().getCommands().getCommandList().get(j) + " ");
//					builder.append(EditorFenster.getInstance().getVrfList().getVrfList().get(i) + "\n");
//					
//				}
//				i++;
//			}
//			EditorFenster.getInstance().insertText(builder.toString());
//		}
//		else{
//			JOptionPane.showMessageDialog(null, "Keine VRF Liste geladen!", "Information Message",
//					JOptionPane.INFORMATION_MESSAGE);
//		}
//	}
	

	/**
	 * getter for vrfList
	 * @return the VrfList
	 */
	public List<String> getVrfList() {
		return vrfList;
	}

	/**
	 * size of List
	 * @return vrfList.size
	 */
	public int VrfListCount() {
		return vrfList.size();
	}

}
