package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import com.editor.EditorFenster;
/**
 * Commands 
 * @author kos
 *
 */
public class Commands {
	
	/**
	 * List to hold Commands
	 */
	private List<String> commandList;
	
	
	/**
	 * read Commands from TextFields and add to commandList Array String
	 * if isEmpty - show MessageDialog()
	 */
	public void readCommands() {
		commandList = new ArrayList<String>();
		try {
			if(!EditorFenster.getInstance().getCommandsPanel().txtCom1.getText().isEmpty()){
				commandList.add(0, EditorFenster.getInstance().getCommandsPanel().txtCom1.getText());
			}
			if(!EditorFenster.getInstance().getCommandsPanel().txtCom2.getText().isEmpty()){
				commandList.add(1, EditorFenster.getInstance().getCommandsPanel().txtCom2.getText());
			}
			if(!EditorFenster.getInstance().getCommandsPanel().txtCom3.getText().isEmpty()){
				commandList.add(2, EditorFenster.getInstance().getCommandsPanel().txtCom3.getText());
			}
			if(!EditorFenster.getInstance().getCommandsPanel().txtCom4.getText().isEmpty()){
				commandList.add(3, EditorFenster.getInstance().getCommandsPanel().txtCom4.getText());
			}
			if(!EditorFenster.getInstance().getCommandsPanel().txtCom5.getText().isEmpty()){
				commandList.add(4, EditorFenster.getInstance().getCommandsPanel().txtCom5.getText());
			}
		} catch (IndexOutOfBoundsException e) {
			JOptionPane.showMessageDialog(null,
			          "Error: Please enter Text from top", "Error Massage",
			          JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	
	/**
	 * getter commandList if not Empty
	 * @return List
	 */
	public List<String> getCommandList(){
		if(commandList.isEmpty() || commandList.equals(null)){
			JOptionPane.showMessageDialog(null, "Command Liste ist leer!", "Warning Massage",
					JOptionPane.WARNING_MESSAGE);
		}
		return commandList;
	}
	
	/**
	 * check if TextField is Empty
	 * @param s String
	 * @return boolean
	 */
	public boolean isEmpty(final String s){
		// Null-safe, short-circuit evaluation.
		  return s == null || s.trim().isEmpty();
	}

	

}
