package com.model;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.editor.EditorFenster;
/**
 * MenueHandler
 * @author kos
 *
 */
public class MenueHandler implements ActionListener {
	private JFileChooser dlg;

	public MenueHandler() {
		dlg = new JFileChooser(".");
		dlg.setFileFilter(new FileNameExtensionFilter("Textdateien", "txt", "log", "xml", "csv"));
//		dlg.addChoosableFileFilter(new FileNameExtensionFilter("Java-Dateien", "java"));
	}

	/**
	 * ActionEvent for Menue case
	 */
	@Override
	public void actionPerformed(ActionEvent ae) {
		String command = ae.getActionCommand().toUpperCase();
		// Datei Öffnen
		switch (command) {
		case "OPEN":
			oeffnen();
			break;
		case "SAVE":
		case "SAVEAS":
			speichern();
			break;
		case "LOAD":
			load();
			break;
		case "EXIT":
			beenden();
			break;
		case "COPY":
			copyClipboard();
			break;
		case "PASTE":
			onPaste();
			break;
		}
	}

	/**
	 * Copy text to clipboard
	 */
	public void copyClipboard(){
		EditorFenster.getInstance().setStatusLabel("Kopieren...");
		String str = EditorFenster.getInstance().getSelectedText();
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Clipboard clipboard = toolkit.getSystemClipboard();
		StringSelection strSel = new StringSelection(str);
		clipboard.setContents(strSel, null);
	}
	
	/**
	 * paste Clipboard to text
	 */
	private void onPaste(){
		EditorFenster.getInstance().setStatusLabel("Einfügen...");
	    Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
	    Transferable t = c.getContents(this);
	    if (t == null)
	        return;
	    try {
	    	EditorFenster.getInstance().insertText((String) t.getTransferData(DataFlavor.stringFlavor));
	    } catch (Exception e){
	        e.printStackTrace();
	    }
	}
	
	/**
	 * load vrfList from File
	 * set results to StatusBar
	 */
	private void load(){
		dlg.setSelectedFile(null);
		if(dlg.showOpenDialog(EditorFenster.getInstance()) == JFileChooser.APPROVE_OPTION){
			File file = dlg.getSelectedFile();
			if(!file.exists()){
				JOptionPane.showMessageDialog(null,
				          "Liste kann nicht geladen werden.", "Error Massage",
				          JOptionPane.ERROR_MESSAGE);
				load();
			}
			else{
				EditorFenster.getInstance().getVrfList().readVrfList(file);
				EditorFenster.getInstance().setStatusLabel("Liste geladen: " + file.getAbsolutePath());
			}
		}
		else{
			EditorFenster.getInstance().setStatusLabel("Keine Liste ausgewählt.");
		}
	}
	
	/**
	 * Method open File
	 */
	private void oeffnen() {
		EditorFenster.getInstance().setStatusLabel("Öffnen...");
    	dlg.setSelectedFile(null);
    	if(dlg.showOpenDialog(EditorFenster.getInstance()) == JFileChooser.APPROVE_OPTION ){
    		// Pfad-Information zum ausgewählten File holen
    		File file = dlg.getSelectedFile();
    		if(!file.exists()){
    			JOptionPane.showMessageDialog(null,
				          "Datei existiert nicht.", "Error Massage",
				          JOptionPane.ERROR_MESSAGE);
    			oeffnen();
    		}
    		else{
    			readFile(file);
    		}
    	}else{
    		EditorFenster.getInstance().setStatusLabel("Keine Datei zum Öffnen ausgewählt.");
    	}
    }
	
	/**
	 * Method to save file
	 */
	 private void speichern() {
		 EditorFenster.getInstance().setStatusLabel("Speichern...");
	    	dlg.setSelectedFile(new File("VrfCommands.txt")); 
	    	if(dlg.showSaveDialog(EditorFenster.getInstance()) == JFileChooser.APPROVE_OPTION){
	    		// Pfad-Information zum ausgewählten File holen
	    		File file = dlg.getSelectedFile();
	    		EditorFenster.getInstance().setStatusLabel("Folgendes File wurde ausgewählt: " + file.getAbsolutePath());
	    		if(file.exists()){
	    			int yesNoCancel = JOptionPane.showConfirmDialog(EditorFenster.getInstance(), "Die Datei existiert bereits, möchtest du überschreiben", "Speichern",
	    					JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
	    			// rückfragen, ob das File überschrieben werden soll
	    			if(yesNoCancel == JOptionPane.NO_OPTION) {
	    				speichern();
	    			}else if(yesNoCancel == JOptionPane.YES_OPTION){
	    				writeFile(file);
	    			}
	    		}else{
	    			writeFile(file);
	    		}
	    	}else{
	    		EditorFenster.getInstance().setStatusLabel("Keine Datei zum Speichern ausgewählt.");
	    	}
	    }

	 /**
	  * Method to close Window
	  * own close Operation NOT used in this Version
	  */
	    public void beenden() {
//	    	System.out.println("Beenden ...");
	    	// in antwort - Nummer des Buttons gespeichert
//	    	int antwort = JOptionPane.showConfirmDialog(f, "Möchtest du wircklich beenden?", "Ende",
//	    			JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
//	    	// wenn Benützer "Yes" angeklickt hat, as Programm beenden
//	    	if(antwort == JOptionPane.YES_OPTION){
//	    	f.dispose();
//	    	}
//	    	// bei mehr optionen mit zB switch (Case "Speichern" Case "Cancel" ...)
//	    	else{
//	    		System.out.println("Der Benützer möchte Programm nicht beenden!");
//	    	}
	    	EditorFenster.getInstance().dispose();
	    }
	    
	    /**
	     *   read file with StreamReader"UTF-8" into char[] - String cast and set with setText
	     * @param filename File
	     */
	    public void readFile(File filename){
	    	try (InputStreamReader reader = new InputStreamReader(new FileInputStream(filename), "UTF-8")) {
				String content;
				long fl = filename.length();
				char [] cbuf = new char [(int) fl];
				reader.read(cbuf);
				content = (String.copyValueOf(cbuf));
				EditorFenster.getInstance().setText(content);
			} catch (Exception e) {
				e.printStackTrace();
			}
	    }
	    
	    /**
	     * write file with OutputStreamReader "UTF-8"
	     * @param filename File
	     */
	    public void writeFile(File filename){
	    	try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(filename), "UTF-8")) {
	    		String content = EditorFenster.getInstance().getText();
	    		writer.write(content);
			} catch (Exception e) {
				e.printStackTrace();
			}
	    }

}
