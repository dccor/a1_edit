package com.util;

/**
 * Performance -Utility Class testing performance
 * with own main
 * @author kos
 *
 */
public class Performance {

	public static void main(String[] args)
    {
        long now = System.currentTimeMillis();
        slow();
        System.out.println("slow elapsed _String concatenation: " + (System.currentTimeMillis() - now) + " ms");

        now = System.currentTimeMillis();
        fast();
        System.out.println("fast elapsed _StringBuilder:		" + (System.currentTimeMillis() - now) + " ms");
    }
	
	
	private static void fast()
    {
        StringBuilder s = new StringBuilder();
        for(int i=0;i<100000;i++)
            s.append("*");      
    }
	
	@SuppressWarnings("unused")
	private static void slow()
    {
        String s = "";
        for(int i=0;i<100000;i++)
            s += "*";
    }
	
	
}
