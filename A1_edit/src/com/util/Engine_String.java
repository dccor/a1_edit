package com.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 * Engine Test - create SampleData text File for TESTING with log4j2
 * @author kos
 *
 */
public class Engine_String {
    /**
     * Defining a static logger variable so that it references the
     * Logger instance named "Engine_String".
     */
    private static final Logger logger = Logger.getLogger(Engine_String.class.getName());
	
    /**
     * logging TEST 
     */
    private static void logging() {
    	BasicConfigurator.configure();
//        logger.entry();
        logger.warn("TEST warn");
        logger.error("TEST error");
//        return logger.exit(false);
        
      }
    
    /**
     * create Random char Array from a-z
     */
	private static StringBuilder sb = new StringBuilder();
	private char[] array = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 
			'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',};
	
	private char getRandomCharacter(){
		return array[random(array.length)];
	}
	
	private int random(int lenght){
		return new Random().nextInt(lenght);
	}
	
	/**
	 * creating String with Random Characters
	 */
	protected Engine_String(){
		boolean running = true;
		int count = 0;
		int max = 5;
		while(running){
			int size = random(25) ;
			for(int i = 0; i < size; i++){
				sb.append(getRandomCharacter());
			}
			sb.append(sb.toString());
//			System.out.println("vrfTest@ " + sb.toString());
			sb.append("\n");
			sb.append("vrf@ ");
			if(count++ == max){
				running = false;
				logger.info("Engine Finished.");
			}
		}
	}
	/**
	 * write File with generated String
	 * @param s
	 * @param f
	 */
	private static void writeFile(String s, File f){
		try {
			FileWriter fw = new FileWriter(f);
			fw.write(s);
			fw.close();
		} catch (IOException e) {
			logger.debug("IOException: ", e);
		}
	}
	
	public static void main(String[] args){
		new Engine_String();
		File file = new File("src/sampleData/VrfSampleData.txt");
		writeFile(sb.toString(), file);
		try {
			logger.info("CanonicalPath: " + file.getCanonicalPath());
		} catch (IOException e) {
			logger.debug("Engine_String IOException: ", e);
		}
		logger.info("memory: " + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()));
//		logging();
	}

}
