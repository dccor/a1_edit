package com.util;

import java.util.ArrayList;
import java.util.List;
/**
 * IterateList Test -UtilityClass for testing
 * own main
 * @author kos
 *
 */
public class IterateList {

	private static List<String> vrfList = new ArrayList<String>();
	private static List<String> commandList = new ArrayList<String>();

	private static void foreach() {
		StringBuilder builder = new StringBuilder();
		for (String comm : commandList) {;
			for (String st : vrfList) {
//				System.out.println(comm + " " + st);
				builder.append(comm + " " + st + "\n");

				// in EditorPane without StringBuilder:
//				EditorFenster.getInstance().appendText(comm + " " + st + "\n");
			}
		}
		System.out.println(builder.toString());
	}

	public static void main(String[] args) {
		vrfList.add(0, "vrfList_1");
		vrfList.add(1, "vrfList__2");
		vrfList.add(2, "vrfList___3");
		commandList.add(0, "ping");
		commandList.add(1, "sh ip");

		foreach();
	}
}
