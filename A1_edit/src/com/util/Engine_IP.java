package com.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.log4j.Logger;
/**
 * Engine_IP TestClass with own log and log4j2
 * @author kos
 *
 */
public class Engine_IP {
	
	private static final Logger logger = Logger.getLogger(Engine_IP.class.getName());

	/**
	 * generate IP Adress from 192.168.1.1 to 192.168.1.254
	 */
	public void generateIP() {
		StringBuilder sb = new StringBuilder();
		int oct1 = 192;
		int oct2 = 168;
		int oct3 = 1;
		String dot = ".";

		for (int i = 1; i < 255; i++) {
			sb.append(oct1);
			sb.append(dot);
			sb.append(oct2);
			sb.append(dot);
			sb.append(oct3);
			sb.append(dot);
			sb.append(i);
			System.out.println(sb.toString());
			sb.setLength(0);
		}
	}

	/**
	 * use the TeePrintStream
	 */
	private static void useTeePrintStream() {
		try {
			FileOutputStream file;
			file = new FileOutputStream("src/sampleData/IPSampleData.txt");
			TeePrintStream tee = new TeePrintStream(file, System.out);
			System.setOut(tee);
		} catch (FileNotFoundException e) {
			logger.debug("Engine_IP Exception: ", e);
		}
	}

	/**
	 * write to log File
	 * @param string - text to log
	 */
	public static void log(String string) {
		PrintWriter out;
		try {
			out = new PrintWriter(new FileWriter("./log_Engine_IP.txt", true), true);
			out.write(string);
			out.close();
		} catch (IOException e) {
			logger.debug("Engine_IP Exception: ", e);
		}
	}

	/**
	 * main Method - useTeePrintStream() , generateIP()
	 * @param args - static main
	 */
	public static void main(String[] args) {
		useTeePrintStream();
		new Engine_IP().generateIP();
		// log(str);
	}

}
